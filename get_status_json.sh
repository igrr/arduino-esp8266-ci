status=$1

if [ "$status" = "pending" ]; then
    description="The test has started"
elif [ "$status" = "success" ]; then
    description="The test has finished successfully"
elif [ "$status" = "failure" ]; then
    description="The test has failed"
elif [ "$status" = "error" ]; then
    description="The test has finished with an error"
else
    echo "usage: $0 <pending|success|failure|error>"
    exit 1
fi

echo "{
    \"state\": \"$status\",
    \"target_url\": \"https://gitlab.com/igrr/arduino-esp8266-ci/pipelines/${CI_PIPELINE_ID}\",
    \"description\": \"$description\",
    \"context\": \"continuous-integration/esp8266-hw-test\"
}"
